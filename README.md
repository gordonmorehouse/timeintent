timeintent
==========

If you gotta store localized time and render it at some other date in a different locale (time zone),
if you do it wrong (i.e. with only UTC), you're gonna have a bad time.

Library for PHP to deal with this, forged from a year of misery at my day job.
